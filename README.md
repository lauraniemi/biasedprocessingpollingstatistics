# README #

### What is this repository for? ###

This repo contains the data and PDFs of the Experiments described in the manuscript "Biased Processing of Political Polling Statistics" 

* Corresponding Author: Laura Niemi, Harvard University 
* Author 2: Mackenna Woodring, Boston College 
* Author 3: Liane Young, Boston College 
* Author 4: Sara Cordes, Boston College 


### Contact ###

* Repo owner: lauraniemi@gmail.com; lauraniemi@fas.harvard.edu